﻿=== 2FA WORDPRESS PLUGIN ===
Contributors: WEBGARH SOLUTIONS - WORDPRESS DEVELOPMENT TEAM
Tags: security, 2fa
Requires at least: 4.7
Tested up to: 4.9
Stable tag: 4.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html




== Description ==
This Plugin is developed to allow admins to force 2 factor authentication on their websites for selected roles. 



== Installation ==

1. Upload `unzipped plugin folder` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress



== Changelog ==

= 1.0.0
* This is first version of 2FA WORDPRESS PLUGIN
