<?php
$date = $month = $year = "";
$all_failed_users_tfa_year = cwebco_get_filter_array_unique_tfa('cwebco_2fa_failed_auth_attempt');
 if(isset($_POST['cwebco_get_filter_tfa_failed_log']))
 {
      $date = $_POST['cwebco_get_filter_value_failed_tfa_date'];
      $month = $_POST['cwebco_get_filter_value_failed_tfa_month'];
      $year = $_POST['cwebco_get_filter_value_failed_tfa_year'];
    $all_failed_users = get_twofa_failed_attempts($date, $month, $year);
 }else{
    $all_failed_users = "";
 }
 $date_array = array("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31");


if(isset($_POST['cwebco_get_filter_activity_log_delete_2fa']))
{
    echo $delete_records = $_POST['cwebco_filter_value_user_activity_delete'];
    $cwebco_get_filter_activity_log_delete = cwebco_get_filter_activity_log_delete_2fa($delete_records, 'cwebco_2fa_failed_auth_attempt');
}
else
{
    $cwebco_get_filter_activity_log_delete="";
}
?>

<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
<script>
jQuery(document).ready(function() {
    jQuery('#data_table_log').DataTable({
        "order": []
    });
} );
</script>

<div class='custom_margin'>
<form style="margin-bottom:10px;" method="post">
<div class="cwebco_get_filter_value_tfa_date">
    <select name="cwebco_get_filter_value_failed_tfa_date">
    <option value="">All Dates</option>
        <?php
        foreach($date_array as $dataa)
        { ?>
            <option <?php if ($date == $dataa) { echo 'selected'; } ?> value="<?php echo $dataa; ?>"><?php echo $dataa; ?></option>
        <?php
        } ?>
    </select>
    <select name="cwebco_get_filter_value_failed_tfa_month">
    <option value="">All Months</option>
    <option <?php if ($month == "01") { echo 'selected'; } ?> value="01">01</option>
    <option <?php if ($month == "02") { echo 'selected'; } ?> value="02">02</option>
    <option <?php if ($month == "03") { echo 'selected'; } ?> value="03">03</option>
    <option <?php if ($month == "04") { echo 'selected'; } ?> value="04">04</option>
    <option <?php if ($month == "05") { echo 'selected'; } ?> value="05">05</option>
    <option <?php if ($month == "06") { echo 'selected'; } ?> value="06">06</option>
    <option <?php if ($month == "07") { echo 'selected'; } ?> value="07">07</option>
    <option <?php if ($month == "08") { echo 'selected'; } ?> value="08">08</option>
    <option <?php if ($month == "09") { echo 'selected'; } ?> value="09">09</option>
    <option <?php if ($month == "10") { echo 'selected'; } ?> value="10">10</option>
    <option <?php if ($month == "11") { echo 'selected'; } ?> value="11">11</option>
    <option <?php if ($month == "12") { echo 'selected'; } ?> value="12">12</option>
    </select>
    <select name="cwebco_get_filter_value_failed_tfa_year">
    <option value="">All Years</option>
        <?php if ($all_failed_users_tfa_year['user_year']){
            foreach($all_failed_users_tfa_year['user_year'] as $data){ ?>
             <option <?php if ($year == $data) { echo 'selected'; } ?> value="<?php echo $data; ?>"><?php echo $data; ?></option>
            <?php }
        } ?>
    </select>
    <input type="submit" name="cwebco_get_filter_tfa_failed_log" class="button" value="Filter"></button>
</div>
</form>
<form name="" id="" method="post">
<div class="class_cwebco_get_filter_activity_log_delete">
    <select name="cwebco_filter_value_user_activity_delete">
    <option value="">Years</option>
    <?php if ($all_failed_users_tfa_year['user_year']){
            foreach($all_failed_users_tfa_year['user_year'] as $data){ ?>
             <option value="<?php echo $data; ?>"><?php echo $data; ?></option>
            <?php }
        } ?>
    </select>
    <input type="submit" name="cwebco_get_filter_activity_log_delete_2fa" class="button" value="Delete Records"></button><?php echo $cwebco_get_filter_activity_log_delete; ?>
</div>
</form>
<button style="float: right;margin: 0px 0px 10px 0px;" onclick="exportTableToCSV('members.csv')" class="cv-btn button">Download CSV</button>
<?php  ?>
<table id="data_table_log" class="display" style="width:100%">
        <thead>
            <tr>
                <th>User Name</th>
                <th>Date:</th>
            </tr>
        </thead>
        <tbody>
        <?php if($all_failed_users){ foreach($all_failed_users as $data){ ?>
        <tr>
            <td><a href="<?php echo home_url(); ?>/wp-admin/user-edit.php?user_id=<?php echo $data->user_id; ?>"> <?php echo $data->username; ?></a></td>
            <td><?php echo $data->created_timestamp; ?></td>
        </tr>
    <?php } } else{ echo '<tr><td>No record found</td><td></td></tr>'; } ?>
        </tbody>
    </table>
    <?php  ?>
</div>
<script>
  function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;
    csvFile = new Blob([csv], {type: "text/csv"});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}
function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("#data_table_log tr");
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        csv.push(row.join(","));        
    }
    downloadCSV(csv.join("\n"), filename);
}
</script>