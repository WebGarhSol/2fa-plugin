<?php
$all_enbled_users = get_twofa_enabled_users();
$all_successfull_users_success_year = cwebco_get_filter_array_unique_tfa('cwebco_2fa_success_auth_attempt');
if(isset($_POST['cwebco_get_filter_activity_log_delete_2fa']))
    {
        $delete_records = $_POST['cwebco_filter_value_user_activity_delete'];
        $cwebco_get_filter_activity_log_delete = cwebco_get_filter_activity_log_delete_2fa($delete_records, 'cwebco_2fa_success_auth_attempt');
    }
?>
 
 <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
<script>
jQuery(document).ready(function() {
    jQuery('#data_table_log').DataTable({
        "order": []
    });
} );
</script>

<button style="float: right;margin: 15px 0px 10px 0px;" onclick="exportTableToCSV('members.csv')" class="cv-btn button">Download CSV</button>
</div>
<div class='custom_margin'>
<table id="data_table_log" class="display" style="width:100%">
        <thead>
            <tr>
                <th>User Name</th>
                <th>Date:</th>
            </tr>
        </thead>
        <tbody>
        <?php if($all_enbled_users){ ?>
        <?php foreach($all_enbled_users as $data){ ?>
        <tr>
            <td><a href="<?php echo home_url(); ?>/wp-admin/user-edit.php?user_id=<?php echo $data->user_id; ?>"> <?php echo $data->username; ?></a></td>
            <td><?php echo $data->created_timestamp; ?></td>
        </tr>
    <?php } }  else{ echo '<tr><td>No record found</td><td></td></tr>'; }  ?>
        </tbody>
    </table>
</div>
<script>
  function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;
    csvFile = new Blob([csv], {type: "text/csv"});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}
function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("#data_table_log tr");
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        csv.push(row.join(","));        
    }
    downloadCSV(csv.join("\n"), filename);
}
</script>