<?php
if(isset($_POST['save_cwebco_tfa_settings']))
{
  update_option('cwebcotfa_settings', $_POST);
}
?>



<?php
global $wp_roles;


$cwebcotfa_settings = get_option('cwebcotfa_settings');
if(isset($cwebcotfa_settings['user_roles_for_cwebcotfa']))
{
  $user_roles_for_cwebcotfa_array=$cwebcotfa_settings['user_roles_for_cwebcotfa'];
}
else
{
  $user_roles_for_cwebcotfa_array=array();
}

if(isset($cwebcotfa_settings['enforece_twofa']))
{
  $enforece_twofa=$cwebcotfa_settings['enforece_twofa'];
}
else
{
  $enforece_twofa="";
}


if(isset($cwebcotfa_settings['twofa_sms_code_length']))
{
  $twofa_sms_code_length=$cwebcotfa_settings['twofa_sms_code_length'];
}
else
{
  $twofa_sms_code_length="";
}




?>
<div class="custom_margin">
    <form method='post'>
      <label><b>Select User Role for 2FA</b></label><br>
            <?php
            $roles = $wp_roles->get_names();
            foreach($roles as $key => $value)
            {
              ?>
              <input type='checkbox' name='user_roles_for_cwebcotfa[]' value='<?php echo $key; ?>' <?php if(in_array($key, $user_roles_for_cwebcotfa_array)){echo "checked";};?> > <?php echo $value; ?> <br>
              <?php
            }
            ?>
            <br><br>
            <label><b>Enforce 2FA</b></label><br>
            <input type='radio' name='enforece_twofa' value='1' <?php if($enforece_twofa==1){echo "checked";}?>> Yes <br>
            <input type='radio' name='enforece_twofa' value='0' <?php if($enforece_twofa==0){echo "checked";}?>> No <br>
            <br><br>
            <label><b>SMS Code Length</b></label><br>
            <input type='number' name='twofa_sms_code_length' value='<?php echo $twofa_sms_code_length; ?>'><br><br>

            <br>
            <label><b></b></label>
            <input type='submit' name='save_cwebco_tfa_settings' value='save' >
          </form>
</div>