<?php
if(isset($_POST['savecwebco_twofa_twiliokey']))
{
    update_option('cwebco_twofa_twilio_key', $_POST['cwebco_twofa_twilio_key']);
    update_option('cwebco_twofa_twilio_token', $_POST['cwebco_twofa_twilio_token']);
    update_option('cwebco_twofa_twilio_number', $_POST['cwebco_twofa_twilio_number']);
    update_option('cwebco_twofa_twilio_alphanumeric_id', $_POST['cwebco_twofa_twilio_alphanumeric_id']);
}
$cwebco_twofa_twilio_key = get_option('cwebco_twofa_twilio_key');
$cwebco_twofa_twilio_token = get_option('cwebco_twofa_twilio_token');
$cwebco_twofa_twilio_number = get_option('cwebco_twofa_twilio_number');
$cwebco_twofa_twilio_alphanumeric_id = get_option('cwebco_twofa_twilio_alphanumeric_id');
?>
<div class="custom_margin">
    <div><i>Two Factor Authentication Twilio key</i></div><br>
        <form method="post" class="form">
            <label>Twilio Account ID: </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input size="40" type="text" name="cwebco_twofa_twilio_key" placeholder="xxxxxxxxxxxxxxxxxxxx" value="<?php echo $cwebco_twofa_twilio_key; ?>"><br><br>
            <label>Twilio Account Token: </label>
            <input size="40" type="text" name="cwebco_twofa_twilio_token" placeholder="xxxxxxxxxxxxxxxxxxxx" value="<?php echo $cwebco_twofa_twilio_token; ?>"><br><br>
            <label>Twilio Number: </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="cwebco_twofa_twilio_number" placeholder="+911234567890" value="<?php echo $cwebco_twofa_twilio_number; ?>"><br><br>
            <label>Alphanumeric ID: </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="cwebco_twofa_twilio_alphanumeric_id" placeholder="ABC123" value="<?php echo $cwebco_twofa_twilio_alphanumeric_id; ?>"><br><br>
            <input type="submit" name="savecwebco_twofa_twiliokey" value="save ">
        </form>
</div>