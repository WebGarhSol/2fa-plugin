<?php
/** @wordpress-plugin
 * Author:            CwebConsultants
 * Author URI:        http://www.cwebconsultants.com/
 */

  global $cwebPluginName;
  global $PluginTextDomain;
        
        $cwebco_twofa_enabledusers_show = $_cwebco_twofa_attempt_success_show = $_cwebco_twofa_attempt_failed_show = 0;
        $cwebco_twofa_enabledusers = $_cwebco_twofa_attempt_success_tab = $_cwebco_twofa_attempt_failed_tab = '';
        
        if(isset($_REQUEST['tab'])){
            if($_REQUEST['tab']=='_cwebco_twofa_enabledusers'){
                $cwebco_twofa_enabledusers = 'nav-tab-active';
                $cwebco_twofa_enabledusers_show = 1;
            }elseif($_REQUEST['tab']=='_cwebco_twofa_attempt_success'){
                $_cwebco_twofa_attempt_success_tab = 'nav-tab-active';
                $_cwebco_twofa_attempt_success_show = 1;
            }elseif($_REQUEST['tab']=='_cwebco_twofa_attempt_failed'){
                $_cwebco_twofa_attempt_failed_tab = 'nav-tab-active';
                $_cwebco_twofa_attempt_failed_show = 1;
            }else{
                $cwebco_twofa_enabledusers = 'nav-tab-active';
                $cwebco_twofa_enabledusers_show = 1;
            }
        }else{
            $cwebco_twofa_enabledusers = 'nav-tab-active';
            $cwebco_twofa_enabledusers_show = 1;
        }
?>

<div class="wrap">
    <h2 class="nav-tab-wrapper">
        <a href="<?=admin_url('admin.php?page=cwebco_twofa_logs&tab=_cwebco_twofa_enabledusers');?>" title="2FA Enabled Users" class="nav-tab <?php echo $cwebco_twofa_enabledusers;?>"><?php _e('2FA Enabled Users');?></a>
        <a href="<?=admin_url('admin.php?page=cwebco_twofa_logs&tab=_cwebco_twofa_attempt_success');?>" title="2FA Success Attempts" class="nav-tab <?php echo $_cwebco_twofa_attempt_success_tab;?>"><?php _e('Success Attempts');?></a>
        <a href="<?=admin_url('admin.php?page=cwebco_twofa_logs&tab=_cwebco_twofa_attempt_failed');?>" title="2FA Failed Attempts" class="nav-tab <?php echo $_cwebco_twofa_attempt_failed_tab;?>"><?php _e('Failed Attempts');?></a>
    </h2>
      
    <?php show_error_message();?>
    <?php

        if($cwebco_twofa_enabledusers_show==1)
        {
          require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/log_tabs/twofa_enabled_users.php';
        }
        elseif($_cwebco_twofa_attempt_success_show==1)
        { 
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/log_tabs/twofa_auth_success_attept.php';
        }
        elseif($_cwebco_twofa_attempt_failed_show==1)
        { 
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/log_tabs/twofa_auth_failed_attept.php';
        }
    ?>
    
        <div class="clear"></div>
</div>
