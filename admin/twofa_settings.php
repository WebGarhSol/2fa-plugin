<?php
/** @wordpress-plugin
 * Author:            CwebConsultants
 * Author URI:        http://www.cwebconsultants.com/
 */

	global $cwebPluginName;
	global $PluginTextDomain;
        
        $about_co_show = $_twilio_key_show = $cwebco_twofa_settings_show = 0;
        $about_co_tab = $_twilio_key_tab = $cwebco_twofa_settings = '';
        
        if(isset($_REQUEST['tab'])){
            if($_REQUEST['tab']=='_about_co'){
                $about_co_tab = 'nav-tab-active';
                $about_co_show = 1;
            }elseif($_REQUEST['tab']=='_twilio_key'){
                $_twilio_key_tab = 'nav-tab-active';
                $_twilio_key_show = 1;
            }else{
                $cwebco_twofa_settings = 'nav-tab-active';
                $cwebco_twofa_settings_show = 1;
            }
        }else{
            $cwebco_twofa_settings = 'nav-tab-active';
            $cwebco_twofa_settings_show = 1;
        }
        
?>

<div class="wrap">
<h2>Two Factor Authentication Settings</h2>
    <h2 class="nav-tab-wrapper">
        <a href="<?=admin_url('admin.php?page=cwebco_twofa&tab=_cwebco_twofa_settings');?>" title="2FA Settings" class="nav-tab <?php echo $cwebco_twofa_settings;?>"><?php _e('SETTINGS');?></a>
         <a href="<?=admin_url('admin.php?page=cwebco_twofa&tab=_twilio_key');?>" title="_twilio_key" class="nav-tab <?php echo $_twilio_key_tab;?>"><?php _e('TWILIO KEY');?></a>
        <a href="<?=admin_url('admin.php?page=cwebco_twofa&tab=_about_co');?>" title="About" class="nav-tab <?php echo $about_co_tab;?>"><?php _e('SUPPORT');?></a>
    </h2>
        
    <?php show_error_message();?>
    <?php

        if($about_co_show==1){
            ?>
            <div class='admin_support_title_outer'>
                <div class='admin_support_title'><h2>2FA Wordpress Plugin</h2></div>
                <div class='admin_support_text'>The plugin allows you to enable 2FA for the selected user roles. The Twilio is being used to send the OTP to the users.</div>
                <br>
                <div class='admin_support_title'><h2>About Team</h2></div>
                <div class='admin_support_text'>Expert WordPress developers building custom solutions on the top of WordPress for established businesses and startups. Our services include…</div>

                <ul class='cwebco_support'>
                  <li>Third party API integrations</li>
                  <li>Custom Plugin development to automate your custom business processes</li>
                  <li>Customization of the website design</li>
                  <li>Customization of the WordPress premium plugin functions</li>
                  <li>Building WooCommerce addons</li>
                </ul>

                <br><br>
                <div class='admin_support_title'><h3>Team Website/Portfolio</h3></div>
                <div class='admin_support_text'>http://wp-plugins.co.in</div>

                <div class='admin_support_title'><h3>Feel free to reach us for any development queries</h3></div>
                <div class='admin_support_text'>info@cwebconsultants.com</div>

                <div class='admin_support_title'><h3>For support, send an email to</h3></div>
                <div class='admin_support_text'>info@cwebconsultants.com</div>
            </div>

        <?php    
        }elseif($cwebco_twofa_settings_show==1){ 
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/setting_tabs/settings.php';
        }
        elseif($_twilio_key_show==1){ 
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/setting_tabs/twilio_settings.php';
        }
    ?>
        <div class="clear"></div>
</div>
