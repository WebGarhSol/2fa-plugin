<?php
/** @wordpress-plugin
 * Author:            CwebConsultants
 * Author URI:        http://www.cwebconsultants.com/
 */
namespace classes_cw;
class Plugin_Activator {
	/* Activate Class */
	public static function activate() {
		global $wpdb;		   
                $sqlQuery="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."cwebco_2fa_enabled_users` (
					   `id` int(11) NOT NULL AUTO_INCREMENT,
                        `user_id` int(11) DEFAULT NULL,
					   `username` varchar(255) DEFAULT NULL,
					   `created_timestamp` timestamp NULL DEFAULT NULL,

                        PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
                $wpdb->query($sqlQuery);

                
            $sqlQuery2="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."cwebco_2fa_success_auth_attempt` (
					   `id` int(11) NOT NULL AUTO_INCREMENT,
                        `user_id` int(11) DEFAULT NULL,
                        `username` varchar(255) DEFAULT NULL,
                        `created_timestamp` timestamp NULL DEFAULT NULL,
					
                        PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
                $wpdb->query($sqlQuery2);

             $sqlQuery3="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."cwebco_2fa_failed_auth_attempt` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `user_id` int(11) DEFAULT NULL,
                        `username` varchar(255) DEFAULT NULL,
                        `created_timestamp` timestamp NULL DEFAULT NULL,
          
                        PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
                $wpdb->query($sqlQuery3);






                // $cwebco_my_account_page = 0;
                // $_for_my_account_page = get_option('_cwebco_my_account_page');
                
                // /* Adding Callback page*/
                // if(!empty($_for_my_account_page)){
                //     if(FALSE === get_post_status( $_for_my_account_page )){
                //         $cwebco_my_account_page = 1;
                //     }
                // }else{
                //     $cwebco_my_account_page = 1;
                // }
                
                // if($cwebco_my_account_page == 1){
                //     $page['post_type']    = 'page';
                //     $page['post_content'] = '';
                //     $page['post_parent']  = 0;
                //     $page['post_author']  = 1;
                //     $page['post_status']  = 'publish';
                //     $page['post_title']   = 'Twofa phone register';
                //     $page['post_name']   = 'twofa_phone_register';

                //     $pageid = wp_insert_post ($page);
                //     add_option( '_cwebco_my_account_page', $pageid );
                // }
                


                // $mobile_verification_page = 0;
                // $_for_mobile_verification_page = get_option('_mobile_verification_page');
                
                // /* Adding Callback page*/
                // if(!empty($_for_mobile_verification_page)){
                //     if(FALSE === get_post_status( $_for_mobile_verification_page )){
                //         $mobile_verification_page = 1;
                //     }
                // }else{
                //     $mobile_verification_page = 1;
                // }
                
                // if($mobile_verification_page == 1){
                //     $page['post_type']    = 'page';
                //     $page['post_content'] = '';
                //     $page['post_parent']  = 0;
                //     $page['post_author']  = 1;
                //     $page['post_status']  = 'publish';
                //     $page['post_title']   = 'Twofa otp verify';
                //     $page['post_name']   = 'Twofa_otp_verify';

                //     $pageid = wp_insert_post ($page);
                //     add_option( '_mobile_verification_page', $pageid );
                // }




                // $cwebco_auth_after_signup_page = 0;
                // $_for_auth_after_signup_page = get_option('_cwebco_auth_after_signup_page');
                
                // /* Adding Callback page*/
                // if(!empty($_for_auth_after_signup_page))
                // {
                //     if(FALSE === get_post_status( $_for_auth_after_signup_page )){
                //         $cwebco_auth_after_signup_page = 1;
                // }
                // }else{
                //     $cwebco_auth_after_signup_page = 1;
                // }
                
                // if($cwebco_auth_after_signup_page == 1){
                //     $page['post_type']    = 'page';
                //     $page['post_content'] = '';
                //     $page['post_parent']  = 0;
                //     $page['post_author']  = 1;
                //     $page['post_status']  = 'publish';
                //     $page['post_title']   = 'Twofa phone Verify';
                //     $page['post_name']   = 'twofa_phone_Verify';

                //     $pageid = wp_insert_post ($page);
                //     add_option( '_cwebco_auth_after_signup_page', $pageid );
                // }










                 //NEWPAGES
                /* Adding auth_after_signuppage page*/
                $auth_after_signuppage_not_exist = 0;
                $_for_auth_after_signuppage_page = get_option('_cwebcotwofa_auth_after_signuppage_form_page');

                /* Adding auth_after_signuppage page*/
                if(!empty($_for_auth_after_signuppage_page)){
                    if(FALSE === get_post_status( $_for_auth_after_signuppage_page )){
                        $auth_after_signuppage_not_exist = 1;
                    }
                }else{
                    $auth_after_signuppage_not_exist = 1;
                }
                
                if($auth_after_signuppage_not_exist == 1){
                    $page['post_type']    = 'page';
                    $page['post_content'] = '';
                    $page['post_parent']  = 0;
                    $page['post_author']  = 1;
                    $page['post_status']  = 'publish';
                    $page['post_title']   = 'TwoFa Auth after Signup';
                    $pageid = wp_insert_post($page);
                    add_option( '_cwebcotwofa_auth_after_signuppage_form_page', $pageid );
                    $templatepath_auth_after_signuppage = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/auth_after_signuppage.php';
                    update_post_meta( $pageid, '_wp_page_template', $templatepath_auth_after_signuppage );
                }









                $change_register_number_not_exist = 0;
                $_for_change_register_number_page = get_option('_cwebcotwofa_change_register_number_form_page');

               /* Adding change_register_numbern page*/
                if(!empty($_for_change_register_number_page)){
                    if(FALSE === get_post_status( $_for_change_register_number_page )){
                        $change_register_number_not_exist = 1;
                    }
                }else{
                    $change_register_number_not_exist = 1;
                }
                
                if($change_register_number_not_exist == 1){
                    $page['post_type']    = 'page';
                    $page['post_content'] = '';
                    $page['post_parent']  = 0;
                    $page['post_author']  = 1;
                    $page['post_status']  = 'publish';
                    $page['post_title']   = 'TwoFa Change Register Number';
                    $pageid = wp_insert_post($page);
                    add_option( '_cwebcotwofa_change_register_number_form_page', $pageid );
                    $templatepath_change_register_number = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/change_register_number.php';
                    update_post_meta( $pageid, '_wp_page_template', $templatepath_change_register_number );
                }










                $cwebco_mobile_verification_not_exist = 0;
                $_for_cwebco_mobile_verification_page = get_option('_cwebcotwofa_cwebco_mobile_verification_form_page');

               /* Adding cwebco_mobile_verification page*/
                if(!empty($_for_cwebco_mobile_verification_page)){
                    if(FALSE === get_post_status( $_for_cwebco_mobile_verification_page )){
                        $cwebco_mobile_verification_not_exist = 1;
                    }
                }else{
                    $cwebco_mobile_verification_not_exist = 1;
                }
                
                if($cwebco_mobile_verification_not_exist == 1){
                    $page['post_type']    = 'page';
                    $page['post_content'] = '';
                    $page['post_parent']  = 0;
                    $page['post_author']  = 1;
                    $page['post_status']  = 'publish';
                    $page['post_title']   = 'TwoFa Mobile Verification';
                    $pageid = wp_insert_post($page);
                    add_option( '_cwebcotwofa_cwebco_mobile_verification_form_page', $pageid );
                    $templatepath_cwebco_mobile_verification = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/cwebco_mobile_verification.php';
                    update_post_meta( $pageid, '_wp_page_template', $templatepath_cwebco_mobile_verification );
                }







                $my_accountpage_not_exist = 0;
                $_for_my_accountpage_page = get_option('_cwebcotwofa_my_accountpage_form_page');

               /* Adding my_accountpage page*/
                if(!empty($_for_my_accountpage_page)){
                    if(FALSE === get_post_status( $_for_my_accountpage_page )){
                        $my_accountpage_not_exist = 1;
                    }
                }else{
                    $my_accountpage_not_exist = 1;
                }
                
                if($my_accountpage_not_exist == 1){
                    $page['post_type']    = 'page';
                    $page['post_content'] = '';
                    $page['post_parent']  = 0;
                    $page['post_author']  = 1;
                    $page['post_status']  = 'publish';
                    $page['post_title']   = 'TwoFa accountpage';
                    $pageid = wp_insert_post($page);
                    add_option( '_cwebcotwofa_my_accountpage_form_page', $pageid );
                    $templatepath_my_accountpage = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/my_accountpage.php';
                    update_post_meta( $pageid, '_wp_page_template', $templatepath_my_accountpage );

                }








                $twofa_thank_you_page_not_exist = 0;
                $_for_twofa_thank_you_page_page = get_option('_cwebcotwofa_twofa_thank_you_page_form_page');

               /* Adding twofa_thank_you_page page*/
                if(!empty($_for_twofa_thank_you_page_page)){
                    if(FALSE === get_post_status( $_for_twofa_thank_you_page_page )){
                        $twofa_thank_you_page_not_exist = 1;
                    }
                }else{
                    $twofa_thank_you_page_not_exist = 1;
                }
                
                if($twofa_thank_you_page_not_exist == 1){
                    $page['post_type']    = 'page';
                    $page['post_content'] = '';
                    $page['post_parent']  = 0;
                    $page['post_author']  = 1;
                    $page['post_status']  = 'publish';
                    $page['post_title']   = 'TwoFa Thank you page';
                    $pageid = wp_insert_post($page);
                    add_option( '_cwebcotwofa_twofa_thank_you_page_form_page', $pageid );
                    $templatepath_twofa_thank_you_page = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/twofa_thank_you_page.php';
                    update_post_meta( $pageid, '_wp_page_template', $templatepath_twofa_thank_you_page );
                }







                $user_myaccount_page_not_exist = 0;
                $_for_user_myaccount_page_page = get_option('_cwebcotwofa_user_myaccount_page_form_page');

               /* Adding user_myaccount_page page*/
                if(!empty($_for_user_myaccount_page_page)){
                    if(FALSE === get_post_status( $_for_user_myaccount_page_page )){
                        $user_myaccount_page_not_exist = 1;
                    }
                }else{
                    $user_myaccount_page_not_exist = 1;
                }
                
                if($user_myaccount_page_not_exist == 1){
                    $page['post_type']    = 'page';
                    $page['post_content'] = '';
                    $page['post_parent']  = 0;
                    $page['post_author']  = 1;
                    $page['post_status']  = 'publish';
                    $page['post_title']   = 'TwoFa User Myaccount';
                    $pageid = wp_insert_post($page);
                    add_option( '_cwebcotwofa_user_myaccount_page_form_page', $pageid );
                    $templatepath_user_myaccount_page = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/user_myaccount_page.php';
                    update_post_meta( $pageid, '_wp_page_template', $templatepath_user_myaccount_page );
                }




    
        }
}
