<?php
/** @wordpress-plugin
 * Author:            CwebConsultants
 * Author URI:        http://www.cwebconsultants.com/
 */

namespace classes_cw;

class Plugin_Deactivator {
	/* De-activate Class */
	public static function deactivate() {
		/* Delete Table And Post type*/
	global $wpdb;	
	$cwebco_2fa_enabled_users="DROP TABLE `".$wpdb->prefix."cwebco_2fa_enabled_users`";
	$wpdb->query($cwebco_2fa_enabled_users);

	$cwebco_2fa_failed_auth_attempt="DROP TABLE `".$wpdb->prefix."cwebco_2fa_failed_auth_attempt`";
		  $wpdb->query($cwebco_2fa_failed_auth_attempt );

	$cwebco_2fa_success_auth_attempt="DROP TABLE `".$wpdb->prefix."cwebco_2fa_success_auth_attempt`";
		  $wpdb->query($cwebco_2fa_success_auth_attempt);
		  	
	}
}