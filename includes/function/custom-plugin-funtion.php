<?php
/** @wordpress-plugin
 * Author:            cWebco WP Plugin Team
 * Author URI:        http://www.cwebconsultants.com/
 */
/* Function for without namespace */

if (!function_exists('set_error_message')) {
    function set_error_message($msg, $type)
    {
        @session_start();
        if (isset($_SESSION['error_msg'])):
            unset($_SESSION['error_msg']);
        endif;
        
        $_SESSION['error_msg']['msg']   = $msg;
        $_SESSION['error_msg']['error'] = $type;
        return true;
    }
}  
function my_login_redirect( $redirect_to, $request, $user ) {
    global $current_user;
    $loggedout="";
    if(!isset($_SESSION)) 
    {
        session_start();
    }
    if(isset($_GET['loggedout']))
    {
        $loggedout=$_GET['loggedout'];
    }
    if( $loggedout == 'true') 
    {
        return $redirect_to;
    }
    else
    {
        if(isset($user->ID))
        {
            $_SESSION['2fa_data_id'] = $user->ID;
            $user_roless         = $user->roles; 
            update_user_meta($user->ID, 'token_id', '');
            update_user_meta($user->ID, '2fa_otp', '');
            $gt_option_value     = get_option('cwebcotfa_settings');
            $gt_option_value     = $gt_option_value["user_roles_for_cwebcotfa"];
            $user_phone_number   = get_user_meta($user->ID, 'user_phone_number', true);
            if (in_array(lcfirst($user_roless[0]), $gt_option_value))
            {
                if(!empty($user_phone_number))
                {
                    do_action( 'twilio_hook_reg', $user_phone_number);
                    $url = get_permalink(get_page_by_path('twofa-mobile-verification'));
                    echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
                    return $url;
                }
                else
                {
                    return get_permalink(get_page_by_path('twofa-accountpage'));
                }
                
            }
            else
            {
                return $redirect_to;
            }
        }
        else
        {
        	return site_url();
        }

    }
}
add_filter( 'login_redirect', 'my_login_redirect' , 10, 3 );

add_action('personal_options_update', 'update_extra_profile_fields');
function update_extra_profile_fields($user_id) {
    update_usermeta($user_id,'user_phone_number',$_POST['user_phone_number']);
 }


// function adding_jquery_migrate()
// {
//     wp_register_script( 'jquery_migrate_3_0_0', 'http://code.jquery.com/jquery-migrate-3.0.0.js', array(),false, 'all');
//     wp_enqueue_script("jquery_migrate_3_0_0");
// }
//add_action('admin_footer','adding_jquery_migrate');


if (!function_exists('show_error_message')) {
    function show_error_message()
    {
        $msg = '';
        @session_start();
        
        if (isset($_SESSION['error_msg']) && isset($_SESSION['error_msg']['msg'])):
            if ($_SESSION['error_msg']['error'] == '1'):
                $tp = 'message_error';
            else:
                $tp = 'message_success';
            endif;
            $msg .= '<div class="portlet light pro_mess"><div class="message center pmpro_message ' . $tp . '">';
            $msg .= $_SESSION['error_msg']['msg'];
            $msg .= '</div></div>';
            unset($_SESSION['error_msg']['msg']);
            unset($_SESSION['error_msg']['error']);
            unset($_SESSION['error_msg']);
        endif;  
        return $msg;
    }
}


if (!function_exists('pr')) {
    function pr($post)
    {
        echo '<pre>';
        print_r($post);
        echo '</pre>';
    }
}


/* Function for Redirect */
if (!function_exists('foreceRedirect')) {
    function foreceRedirect($filename)
    {
        if (!headers_sent())
            header('Location: ' . $filename);
        else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $filename . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
            echo '</noscript>';
        }
    }
}
/* Function for Redirect --ENDS */

if (!function_exists('get_twofa_enabled_users')) {
    function get_twofa_enabled_users()
    {
        global $wpdb;
        $query  = "select * from `" . $wpdb->prefix . "cwebco_2fa_enabled_users` GROUP BY username ORDER BY `id` DESC ";
        $result = $wpdb->get_results($query);
        return $result;
    }
}

if (!function_exists('get_twofa_successfull_attempts')) {
    function get_twofa_successfull_attempts($date, $month, $year)
    {
        global $wpdb;
        $query  .= "select * from `" . $wpdb->prefix . "cwebco_2fa_success_auth_attempt` ";
        $query  .= "WHERE id != '' ";
        if ($date != null)          {
            $date = "-".$date." ";
            $query  .= "AND created_timestamp LIKE '%$date%' "; }
        if ($month != null)          {
            $month = "-".$month."-";
            $query  .= "AND created_timestamp LIKE '%$month%' "; }
        if ($year != null)          { $query  .= "AND created_timestamp LIKE '$year%' "; }
        $query  .= "ORDER BY created_timestamp DESC";
        $result = $wpdb->get_results($query);
        return $result;
    }
}
if (!function_exists('cwebco_get_filter_activity_log_delete_2fa')) {   
    function cwebco_get_filter_activity_log_delete_2fa($delete_records, $table)
    {
        global $wpdb;
        if($delete_records){
        header("Refresh: 0;");
        $table_name = $wpdb->prefix.$table;
        $result = $wpdb->query( "DELETE  FROM {$table_name} WHERE created_timestamp LIKE '$delete_records%'");
        if($result > 0 ){
        }else{
            return '<P STYLE="color:red;"> Please select year.</p>';   
        }
        }else{
            return '<P STYLE="color:red;"> Please select year.</p>';
        }
    }
}
if (!function_exists('get_twofa_failed_attempts')) {
    function get_twofa_failed_attempts($date, $month, $year)
    {
        global $wpdb;
        $query="";
        $query.= "select * from `" . $wpdb->prefix . "cwebco_2fa_failed_auth_attempt` ";
        $query.= "WHERE id != '' ";
        if ($date != null)          {
            $date = "-".$date." ";
            $query  .= "AND created_timestamp LIKE '%$date%' "; }
        if ($month != null)          {
            $month = "-".$month."-";
            $query  .= "AND created_timestamp LIKE '%$month%' "; }
        if ($year != null)          { $query  .= "AND created_timestamp LIKE '$year%' "; }
        $query  .= "ORDER BY created_timestamp DESC";
        $result = $wpdb->get_results($query);
        return $result;
    }
}

 
if (!function_exists('cwebco_get_filter_array_unique_tfa')) {
    function cwebco_get_filter_array_unique_tfa($table)
    {
        global $wpdb;
        $sk_result_array_unique_tfa = array();
        $query  = "select DISTINCT YEAR(created_timestamp) user_year from `" . $wpdb->prefix . "$table` ORDER BY created_timestamp DESC ";
        $user_year = $wpdb->get_results($query);
        if ($user_year){
            foreach($user_year as $data){
                $sk_result_array_unique_tfa['user_year'][] = $data->user_year;
            }
        }

        return $sk_result_array_unique_tfa;
    }
}

if (!function_exists('validate_phone_number')) {
    function validate_phone_number($phone)
    {
        $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
        $phone_to_check        = str_replace("+", "", $filtered_phone_number);
        if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
            return false;
        } else {
            return $phone_to_check;
        }
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 6)
    {
        $characters       = '0123456789';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}


add_action('twilio_hook_reg', 'twilio_message_tfo');
function twilio_message_tfo($mobl_nmber_fortwofa)
{
    if(!isset($_SESSION)) 
    {
        session_start();
    }
    global $current_user, $wpdb;
    $current_user_id = get_current_user_id();
    if (!$current_user_id) {
        $current_user_id = $_SESSION['2fa_data_id'];
    }
    if(isset($_SESSION['2fa_mobile_change']))
    {
        $change_user_num = $_SESSION['2fa_mobile_change'];
    }
    else
    {
         $change_user_num = "";
    }


   
    $gt_option_value = get_option('cwebcotfa_settings');
    $mob_number      = validate_phone_number($mobl_nmber_fortwofa);
    if ($mob_number != '') {
        $twilio_api_path = plugin_dir_path(__FILE__) . 'sms/vendor/autoload.php';
        require_once($twilio_api_path);
        $rndnom        = generateRandomString($gt_option_value["twofa_sms_code_length"]);
        $account_sid   = get_option('cwebco_twofa_twilio_key');
        $auth_token    = get_option('cwebco_twofa_twilio_token');
        $twilio_number = get_option('cwebco_twofa_twilio_number');
        $alphanumeric_id = get_option('cwebco_twofa_twilio_alphanumeric_id');
        if(!empty($alphanumeric_id)){
            $twilio_number = $alphanumeric_id;   
        }

        $number        = "+" . $mob_number;
        $sms           = 'Your SMS OTP is ' . $rndnom . '. Use it within 2 minutes before it expires.';
        $client        = new Twilio\Rest\Client($account_sid, $auth_token);
        try {
            $client->messages->create($number, array(
                'from' => $twilio_number,
                'body' => $sms
            ));
            $rndnom1 = base64_encode($rndnom);
            update_user_meta($current_user_id, '2fa_otp', $rndnom1);
            if (isset($_POST['change_mobile_num_again'])) {
            } else {
                update_user_meta($current_user_id, 'user_phone_number', $number);
            }
        }
        catch (Exception $e) {

                    echo "e is ";
        echo "<pre>";
        print_r($e);
        die;
        
        }
    }
}


add_action( 'edit_user_profile_update', 'wk_save_custom_user_profile_fields' );
/**
*   @param User Id $user_id
*/
function wk_save_custom_user_profile_fields( $user_id )
{   
    $current_user_id = get_current_user_id();
    $custom_data = $_POST['user_phone_number'];
    update_user_meta( $user_id, 'user_phone_number', $custom_data );
}

if (!function_exists('mylog_out')) {
function mylog_out()
{
    global $current_user, $wp;
    $token = wp_get_session_token();
    if ($token) {
        $manager = WP_Session_Tokens::get_instance(get_current_user_id());
        $manager->destroy($token);
    }
    update_user_meta(get_current_user_id(), 'token_id', '');
    update_user_meta(get_current_user_id(), '2fa_otp', '');
    unset($_SESSION['2fa_data_id']);
}
add_filter('wp_logout','mylog_out');
}


if (!function_exists('cwebco_get_filter_tfa')) {
    function cwebco_get_filter_tfa()
        {
            global $current_user, $wpdb;
        }
add_action( 'wp_ajax_nopriv_cwebco_get_filter_tfa', 'cwebco_get_filter_tfa' );
add_action( 'wp_ajax_cwebco_get_filter_tfa', 'cwebco_get_filter_tfa' );
}



