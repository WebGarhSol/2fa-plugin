<?php
/** @wordpress-plugin
 * Author:            cWebco WP Plugin Team
 * Author URI:        http://www.cwebconsultants.com/
 */
/* Function for session message */

namespace tfafunctionamespace {
add_action('init', __NAMESPACE__ . '\\theme_name_scripts');
function theme_name_scripts()
{
    
    if (!is_admin() && $GLOBALS['pagenow'] != 'wp-login.php') {
        $component_css_path = CWEB_TFADU_PATH . 'public/assets/css/components.css';
        wp_enqueue_style('components', $component_css_path);
        
        
        
        $component_css_path2 = 'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css';
        wp_enqueue_style('bootstrapcss', $component_css_path2);
 
    }
}


if (!function_exists('do_account_redirect')) {
    function do_account_redirect($url)
    {
        global $post, $wp_query;
        
        if (have_posts()) {
            include($url);
            die();
        } else {
            $wp_query->is_404 = true;
        }
    }
}


//add_action('template_redirect', __NAMESPACE__ . '\\account_page_redirect1');
function account_page_redirect1()
{
    global $wp, $post;
    $page_slug = $post->post_name;
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    if ($page_slug == "twofa_phone_register") {
        if (file_exists(CWEB_TFADP_PATH . 'public/templates/auth_after_signuppage.php')) {
            $return_template = CWEB_TFADP_PATH . 'public/templates/auth_after_signuppage.php';
            do_account_redirect($return_template);
        }
    } elseif ($page_slug == "twofa_phone_verify") {
        if (file_exists(CWEB_TFADP_PATH . 'public/templates/auth_after_signuppage.php')) {
            $return_template = CWEB_TFADP_PATH . 'public/templates/auth_after_signuppage.php';
            do_account_redirect($return_template);
        }
    } elseif ($page_slug == "twofa_otp_verify") {
        if (file_exists(CWEB_TFADP_PATH . 'public/templates/cwebco_mobile_verification.php')) {
            $return_template = CWEB_TFADP_PATH . 'public/templates/cwebco_mobile_verification.php';
            do_account_redirect($return_template);
        }
    } elseif ($page_slug == "user_my_account_page") {
        if (file_exists(CWEB_TFADP_PATH . 'public/templates/user_myaccount_page.php')) {
            $return_template = CWEB_TFADP_PATH . 'public/templates/user_myaccount_page.php';
            do_account_redirect($return_template);
        }
    } elseif ($page_slug == "change_register_number") {
        if (file_exists(CWEB_TFADP_PATH . 'public/templates/change_register_number.php')) {
            $return_template = CWEB_TFADP_PATH . 'public/templates/change_register_number.php';
            do_account_redirect($return_template);
        }
    } elseif ($page_slug == "twofa_thank_you_page") {
        if (file_exists(CWEB_TFADP_PATH . 'public/templates/twofa_thank_you_page.php')) {
            $return_template = CWEB_TFADP_PATH . 'public/templates/twofa_thank_you_page.php';
            do_account_redirect($return_template);
        }
    }
}

 
add_action('wp_head', __NAMESPACE__ . '\\add_meta_admin');
function add_meta_admin()
{
    $admin_meta = get_user_meta(1, 'user_2fa_enabled', true);
    if (!$admin_meta) {
        update_user_meta(1, 'user_2fa_enabled', 'no');
        update_user_meta(1, 'user_phone_verified', 'no');
        update_user_meta(1, 'user_phone_number', '');
    }
}


if (!function_exists('insert_update_user_2fa_fun')) {
add_action('insert_update_user_2fa', __NAMESPACE__ . '\\insert_update_user_2fa_fun');
function insert_update_user_2fa_fun($table_name_2fa)
{
    if(!isset($_SESSION))
    {
        session_start();
    }
    global $current_user, $wpdb;
    $current_user_id = get_current_user_id();
    if (is_user_logged_in()) {
        $author_obj = get_user_by('id', $current_user_id);
        $username   = $author_obj->data->user_nicename;
        $userid     = $author_obj->data->ID;
    } else {
        $current_user_id = $_SESSION['2fa_data_id'];
        $author_obj      = get_user_by('id', $current_user_id);
        $username        = $author_obj->data->user_nicename;
        $userid          = $author_obj->data->ID;
    }

    $datesent   = date('Y-m-d H:i:s', time());
    $wpdb->insert($table_name_2fa, array(
                "user_id" => $userid,
                "username" => $username,
                "created_timestamp" => $datesent
        ));
}
}


add_action('user_register', __NAMESPACE__ . '\\custom_user_meta_2fa', 10, 1);
function custom_user_meta_2fa($user_id)
{
    if(!isset($_SESSION))
    {
        session_start();
    }
    if(isset( $_POST['user_phone_number']))
    {
        $user_phone_number = $_POST['user_phone_number'];
    }
    else
    {
        $user_phone_number="";
    }
    update_user_meta($user_id, 'user_2fa_enabled', 'no');
    update_user_meta($user_id, 'user_phone_verified', 'no');
    update_user_meta($user_id, 'user_phone_number', $user_phone_number);
    $current_user_id = get_current_user_id();
    $user_info               = get_userdata($user_id);
    $userrole                = implode(', ', $user_info->roles);
    $_SESSION['2fa_data_id'] = $user_id;
    $gt_option_value         = get_option('cwebcotfa_settings');
    $current_user = wp_get_current_user();
    if(isset($current_user->roles[0]))
    {
        if($current_user->roles[0] != 'administrator' ){
            if (in_array(lcfirst($userrole), $gt_option_value["user_roles_for_cwebcotfa"])) {
                $url = get_permalink(get_page_by_path('twofa-auth-after-signup'));
                echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
                exit;
            }
        }
    }
}



add_action('init', __NAMESPACE__ . '\\forced_redirect_notverify');
function forced_redirect_notverify()
{
    global $wp_query, $current_user, $wp, $post;
    if($post)
    {
        $post_slug                      = $post->post_name;
        $gt_option_value                = array();
        $gt_option_user_rolesfortwofa   = array();
        if (is_user_logged_in()) {
            $current_link                       = explode('/', $_SERVER['REQUEST_URI']);
            $current_link2                      = explode('=', $current_link[2]);
            $current_user_id                    = get_current_user_id();
            $user_roless                        = $current_user->roles;
            $user_phone_number                  = get_user_meta($current_user_id, 'user_phone_number', true);
            $session_tokens                     = get_user_meta($current_user_id, 'session_tokens', true);
            $user_phone_verified                = get_user_meta($current_user_id, 'user_phone_verified', true);
            $gt_option_value                    = get_option('cwebcotfa_settings');
            $gt_option_user_rolesfortwofa       = $gt_option_value["user_roles_for_cwebcotfa"];
            $enforece_twofa                     = $gt_option_value['enforece_twofa'];
            $token_id                           = get_user_meta($current_user_id, 'token_id', true);
            $token                              = wp_get_session_token();




            if($enforece_twofa=="1")
            {
                if (in_array(lcfirst($user_roless[0]), $gt_option_user_rolesfortwofa)) {
                    if (($user_phone_number == '') AND ($user_phone_verified == 'no')) {
                        if (!in_array('twofa-auth-after-signup', $current_link)) {
                            if (!in_array('logout&_wpnonce', $current_link2)) {
                                $url = get_permalink(get_page_by_path('twofa-auth-after-signup'));
                                echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
                                exit;
                            }
                        }
                    } elseif (($user_phone_number != '') AND ($user_phone_verified == 'no')) {
                        if (!in_array('twofa-mobile-verification', $current_link)) {
                            if (!in_array('logout&_wpnonce', $current_link2)) {
                                $url = get_permalink(get_page_by_path('twofa-mobile-verification'));
                                echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
                                exit;
                            }
                        }
                    } else {
                        if (($user_phone_verified == 'yes') AND ($token_id == '')) {
                            if (!in_array('twofa-mobile-verification', $current_link)) {
                                if (!in_array('logout&_wpnonce', $current_link2)) {
                                    $url = get_permalink(get_page_by_path('twofa-mobile-verification'));
                                    echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
                                    exit;
                                }
                            }
                        }
                    }
                }
            }








            // if (in_array(lcfirst($user_roless[0]), $gt_option_user_rolesfortwofa)) {
            //     if (($user_phone_number == '') AND ($user_phone_verified == 'no')) {
            //         if (!in_array('twofa-auth-after-signup', $current_link)) {
            //             if (!in_array('logout&_wpnonce', $current_link2)) {
            //                 $url = get_permalink(get_page_by_path('twofa-auth-after-signup'));
            //                 echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
            //                 exit;
            //             }
            //         }
            //     } elseif (($user_phone_number != '') AND ($user_phone_verified == 'no')) {
            //         if (!in_array('twofa-mobile-verification', $current_link)) {
            //             if (!in_array('logout&_wpnonce', $current_link2)) {
            //                 $url = get_permalink(get_page_by_path('twofa-mobile-verification'));
            //                 echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
            //                 exit;
            //             }
            //         }
            //     } else {
            //         if (($user_phone_verified == 'yes') AND ($token_id == '')) {
            //             if (!in_array('twofa-mobile-verification', $current_link)) {
            //                 if (!in_array('logout&_wpnonce', $current_link2)) {
            //                     $url = get_permalink(get_page_by_path('twofa-mobile-verification'));
            //                     echo '<script language="javascript">window.location.href ="' . $url . '"</script>';
            //                     exit;
            //                 }
            //             }
            //         }
            //     }
            // }




            
        } // if user loggedin
    }
}

add_action('admin_init', __NAMESPACE__ . '\\my_remove_menu_pages');
function my_remove_menu_pages()
{
    global $user_ID, $current_user, $pagenow;
    $user_id = get_current_user_id();
    if ($current_user->roles[0] != 'administrator') {
        remove_menu_page('admin.php?page=cwebco_twofa');
        remove_menu_page('admin.php?page=cwebco_twofa_logs');
        remove_menu_page('cwebco_twofa');
        if ($pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'cwebco_twofa') {
            wp_redirect(admin_url('/post-new.php?post_type=page', 'http'), 301);
            exit;
        }
        if ($pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'cwebco_twofa_logs') {
            wp_redirect(admin_url('/post-new.php?post_type=page', 'http'), 301);
            exit;
        }
    }
}

if (!function_exists('insert_update_user_2fa_enb_usr')) {
add_action('insert_update_user_2fa_enb', __NAMESPACE__ . '\\insert_update_user_2fa_enb_usr');
function insert_update_user_2fa_enb_usr($table_name_2fa)
{
    session_start();
    global $current_user, $wpdb;
    $current_user_id = get_current_user_id();
    if (is_user_logged_in()) {
        $author_obj = get_user_by('id', $current_user_id);
        $username   = $author_obj->data->user_nicename;
        $userid     = $author_obj->data->ID;
    } else {
        $current_user_id = $_SESSION['2fa_data_id'];
        $author_obj      = get_user_by('id', $current_user_id);
        $username        = $author_obj->data->user_nicename;
        $userid          = $author_obj->data->ID;
    }
    $datesent   = date('Y-m-d H:i:s', time());
    $query  = "select * from `" . $wpdb->prefix . "cwebco_2fa_enabled_users` where `user_id` = $current_user_id ORDER BY `id` DESC ";
        $enable_userid = $wpdb->get_results($query);
    if(empty($enable_userid)){
        $wpdb->insert($table_name_2fa, array(
                "user_id" => $userid,
                "username" => $username,
                "created_timestamp" => $datesent
        ));
    } 
}
}

function custom_user_profile_fields($profileuser)
{

    $twofa_status             = "";
    $twofa_number             = "";
    $user_phone_verified      = "";

    if(is_object($profileuser))
    {
        $twofa_status           = get_the_author_meta('user_2fa_enabled', $profileuser->ID);
        $twofa_number           = get_the_author_meta('user_phone_number', $profileuser->ID);
        $user_phone_verified    = get_the_author_meta('user_phone_verified', $profileuser->ID);
    }


  ?>
<table class="form-table">
    <tr>
        <th>
            <label for="user_location"> <?php esc_html_e('2FA Status'); ?></label>
        </th>
        <td>
            <input type="text" name="user_2fa_enabled" id="user_2fa_enabled" value="<?php  echo esc_attr( $twofa_status); ?>" class="regular-text" readonly />
        </td>
    </tr>
    <tr>
        <th>
            <label for="user_location"><?php esc_html_e('Phone Number'); ?>
            </label>
        </th>
        <td>
            <input type="text" name="user_phone_number" id="user_phone_number" value="<?php echo esc_attr($twofa_number); ?>" class="regular-text"  />
            <span class="description"><?php esc_html_e( 'Phone Number Should be like (+919876543210)', 'text-domain' ); ?></span>
        </td>
    </tr>
    <tr>
        <th>
            <label for="user_location"><?php esc_html_e('Phone Number verified'); ?></label>
        </th>
        <td>
            <input type="text" name="user_phone_verified" id="user_phone_verified" value="<?php echo esc_attr($user_phone_verified); ?>" class="regular-text" readonly/>
        </td>
    </tr>
</table>
<?php
}
add_action('show_user_profile', __NAMESPACE__ . '\\custom_user_profile_fields');
add_action('edit_user_profile', __NAMESPACE__ . '\\custom_user_profile_fields');
add_action( 'user_new_form', __NAMESPACE__ . '\\custom_user_profile_fields' );


if (!function_exists('custom_user_profile_logout')) {
    function custom_user_profile_logout()
    { ?>
        <script>
        jQuery( document ).ready(function() {
            jQuery(document).on('click', '#wp-admin-bar-logout', function(){
            console.log('timer_over');
            localStorage.setItem('timer_over','abc');
            localStorage.removeItem("timer_over");
          });
        });
        </script>
    <?php }
    add_action( 'wp_footer', __NAMESPACE__ . '\\custom_user_profile_logout');
}







//Load template from specific page
if (!function_exists('cwebco_twofa_page_template')) {
    add_filter( 'page_template', __NAMESPACE__ . '\\cwebco_twofa_page_template' );
    function cwebco_twofa_page_template( $page_template ){

        $templatepath_auth_after_signuppage = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/auth_after_signuppage.php';
        if ( get_page_template_slug() == $templatepath_auth_after_signuppage ) {
            $page_template = $templatepath_auth_after_signuppage;
        }

        $templatepath_change_register_number = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/change_register_number.php';
        if ( get_page_template_slug() == $templatepath_change_register_number ) {
            $page_template = $templatepath_change_register_number;
        }

        $templatepath_cwebco_mobile_verification = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/cwebco_mobile_verification.php';
        if ( get_page_template_slug() == $templatepath_cwebco_mobile_verification ) {
            $page_template = $templatepath_cwebco_mobile_verification;
        }

        $templatepath_my_accountpage = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/my_accountpage.php';
        if ( get_page_template_slug() == $templatepath_my_accountpage ) {
            $page_template = $templatepath_my_accountpage;
        }

        $templatepath_twofa_thank_you_page = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/twofa_thank_you_page.php';
        if ( get_page_template_slug() == $templatepath_twofa_thank_you_page ) {
            $page_template = $templatepath_twofa_thank_you_page;
        }

        $templatepath_user_myaccount_page = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/user_myaccount_page.php';
        if ( get_page_template_slug() == $templatepath_user_myaccount_page ) {
            $page_template = $templatepath_user_myaccount_page;
        }

        return $page_template;
    }
}
/**
 * Add "Custom" template to page attirbute template section.
 */
if (!function_exists('cwebco_twofa_add_template_to_select')) {
    add_filter( 'theme_page_templates', __NAMESPACE__ . '\\cwebco_twofa_add_template_to_select', 10, 4 );
    function cwebco_twofa_add_template_to_select( $post_templates, $wp_theme, $post, $post_type ) {

        $templatepath_auth_after_signuppage = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/auth_after_signuppage.php';
        $post_templates[$templatepath_auth_after_signuppage] = __('Twofa Auth After Signup');

        $templatepath_change_register_number = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/change_register_number.php';
        $post_templates[$templatepath_change_register_number] = __('Twofa Change Register Number');

        $templatepath_cwebco_mobile_verification = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/cwebco_mobile_verification.php';
        $post_templates[$templatepath_cwebco_mobile_verification] = __('Twofa Mobile Verification');

        $templatepath_my_accountpage = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/my_accountpage.php';
        $post_templates[$templatepath_my_accountpage] = __('Twofa My account');

        $templatepath_twofa_thank_you_page = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/twofa_thank_you_page.php';
        $post_templates[$templatepath_twofa_thank_you_page] = __('Twofa Thank You');

        $templatepath_user_myaccount_page = WP_PLUGIN_DIR.'/twofa_cwebco/public/templates/user_myaccount_page.php';
        $post_templates[$templatepath_user_myaccount_page] = __('Twofa User Myaccount');



        return $post_templates;
    }
}




add_action('wp_footer', __NAMESPACE__ . '\\dfgsdfgdfgdsfg');
function dfgsdfgdfgdsfg()
{
    global $wp, $post;
    echo " ___ ";
    echo $url = get_permalink(get_page_by_path('twofa-mobile-verification'));
    echo " ___ ";
     $page_slug = $post->post_name;
}

} // namespace







