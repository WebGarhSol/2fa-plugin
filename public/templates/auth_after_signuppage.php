<?php
/**
 * Template Name: Twofa Auth After Signup
 */


get_header();
if(!isset($_SESSION)) 
{ 
  session_start();
}
$current_user_id = get_current_user_id();
$token = wp_get_session_token();
global $wpdb;

if ( is_user_logged_in() )
{ 
 $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
 $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
 $tfa_saved_otp = base64_decode($tfa_saved_otp1);
}else{
 $url = get_option('siteurl') . '';
    echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
    header("location:".$url);
 $current_user_id = $_SESSION['2fa_data_id'];
 $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
 $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
 $tfa_saved_otp = base64_decode($tfa_saved_otp1);
} 

$msg1="";

// Resend otp to number again
if(isset($_POST['resend_code_to_verify_twofa']))
{ 
  do_action( 'twilio_hook_reg', $tfa_mobile_no);
  echo "<script>localStorage.setItem('timer_over','');</script>";
}

// verify otp and enable status
if(isset($_POST['get_code_to_verify_twofa']))
{
  if ( !empty($_POST['mobl_nmber_fortwofa_recieved_code'] ))
  {
   $mobile_otp =  $_POST['mobl_nmber_fortwofa_recieved_code'];
    if($tfa_saved_otp == $mobile_otp){
      $table_name = $wpdb->prefix.'cwebco_2fa_success_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
      $table_name2 = $wpdb->prefix.'cwebco_2fa_enabled_users';
      do_action( 'insert_update_user_2fa_enb', $table_name2);
      update_user_meta( $current_user_id, 'user_2fa_enabled', 'yes');
      update_user_meta( $current_user_id, 'user_phone_verified', 'yes');
      update_user_meta( $current_user_id, 'token_id', $token);
      unset($_SESSION['2fa_data_id']);
      unset($_SESSION['2fa_mobile_no']);
      if($current_user_id == 1){
        $url = get_option('siteurl') . '/wp-admin/index.php';
      }else{
        //$url = get_option('siteurl') . '/wp-admin/profile.php';
        $url = get_option('siteurl') . '/twofa-thank-you-page/';
      }
      echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
      header("location:".$url);
    }else{
      update_user_meta( $current_user_id, '2fa_otp', '');
      $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
      $msg1 .= "<span id='msg_ids' style='color:red;' >OTP not correct.</span>";
    }   
  }
  else
  {
    $msg1 .= "<span id='msg_ids' style='color:red;' >Field Not Be Empty.</span>";
  }
}
else
{
  $msg1=" ";
}


?>

<div class="custom_page_class">
<div class="center_back_otp">    

<!-- form verify mobile number again -->

<!-- form verify otp and enable status -->
<form name="hide_after_60" id="hide_after_60" class="text_otp_cent" method="post">
  <div class="form-group">  
    <label for="verifymobile"><b>Enter the 6-digit One Time Password (OTP) sent to your mobile number</b></label>
    <input id="mobl_nmber_fortwofa_recieved_code" type="text" class="form-control" name="mobl_nmber_fortwofa_recieved_code" >
    <?php echo $msg1; ?>
    <span id="countdown-1">120</span><span id="cunt_seconds"> seconds</span>  
  </div>
  <div class="btn_vrify">    
    <input type="submit" name="get_code_to_verify_twofa" class="btn btn-primary verify_mob" value="Submit">
  </div>      
   
  <br>
</form>
<!-- form Resend otp to number again -->
<form id="resend_otp" method="post">
<div class="btn_vrify">     
    <input type="submit" id="resend_otp_trigger" name="resend_code_to_verify_twofa" class="btn btn-primary verify_mob" value="Resend OTP"></div>
</form> 

</div>
</div>
<script type="text/javascript">
var time_over = localStorage.getItem("timer_over");
    if(time_over != "usercreate"){
    secs       = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock      = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = clock;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
            setTimeout(localStorage.setItem("timer_over","usercreate"),1000);
        } else {
          localStorage.setItem("timer_over","usercreate");
            document.getElementById(id).innerHTML = "Please resend OTP again";
            document.getElementById('cunt_seconds').style.display = 'none';
            var meta_id = '<?php echo $current_user_id; ?>'; 
            jQuery.ajax({
              type: 'post',
              data: {meta_id: meta_id},
              success: function(response){
                // Code
              }
            });
        }
    }
    }else{
      document.getElementById("cunt_seconds").innerHTML = "Please resend OTP again";
      document.getElementById('countdown-1').style.display = 'none';
      var meta_id = '<?php echo $current_user_id; ?>'; 
            jQuery.ajax({
              type: 'post',
              data: {meta_id: meta_id},
              success: function(response){
                // Code
              }
            });
    }
   

jQuery(document).ready(function () {
  setTimeout(function () {
    jQuery('#msg_ids').hide();
  }, 8000);
});
jQuery( document ).ready(function() {
            jQuery(document).on('click', '#wp-admin-bar-logout', function(){
            console.log('timer_over');
            localStorage.setItem('timer_over','abc');
            localStorage.removeItem("timer_over");
          });
        });
</script>




<?php 
if(isset($_POST['meta_id']) ){
  $delete_user_m_id = $_POST['meta_id'];
  update_user_meta( $delete_user_m_id, '2fa_otp', '');
  exit;
 }
wp_footer(); ?>
