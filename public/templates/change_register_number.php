<?php
/**
 * Template Name: Twofa Change Register Number
 */
get_header();
if(!isset($_SESSION)) 
{
  session_start();
}
$current_user_id = get_current_user_id();
$token = wp_get_session_token();
$msg1="";
if ( is_user_logged_in() )
{
 $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
 $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
 $tfa_saved_otp = base64_decode($tfa_saved_otp1);
}else{
  if(!isset($_SESSION['2fa_data_id']))
  {
    wp_redirect(site_url());
    exit;
  }
 $current_user_id = $_SESSION['2fa_data_id'];
 $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
 $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
 $tfa_saved_otp = base64_decode($tfa_saved_otp1);
}
    
//verify mobile number again 
if(isset($_POST['change_mobile_num_again']))
{ 
  if (!empty( $_POST['get_change_mobile_num'] ))
  { 
    if(preg_match("/^[+]{1}[01234567890]{2}[0-9]{10}$/", $_POST['get_change_mobile_num'])) {
    $_SESSION['2fa_mobile_change'] = $_POST['get_change_mobile_num'];
    do_action( 'twilio_hook_reg', $_POST['get_change_mobile_num']); 
  }else{
    $msg1 .= "<span id='msg_ids' style='color:red;' >Invalid phone number</span>";
  }
  }else{
    $msg1 .= "<span id='msg_ids' style='color:red;' >Field Not Be Empty</span>";
  }
}


// verify otp and enable status
if(isset($_POST['get_code_to_verify_twofa']))
{ 
  if ( !empty($_POST['mobl_nmber_fortwofa_recieved_code'] ))
  { 
    $mobile_otp =  $_POST['mobl_nmber_fortwofa_recieved_code'];
    if($tfa_saved_otp == $mobile_otp){
      $phone_num = $_SESSION['2fa_mobile_change'];
      update_user_meta( $current_user_id, 'user_phone_number', $phone_num);
      unset($_SESSION['2fa_mobile_change']);
      $url = home_url( 'index.php' ).'';
      echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
    }else{
      $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
      $msg1 .= "<span id='msg_ids' style='color:red;' >Otp not verified</span>";
    }   
  }else{
    $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
    $msg1 .= "<span id='msg_ids' style='color:red;' >Field Not Be Empty</span>";  
  }
}

?>
<div class="custom_page_class">
<div class="center_back_otp">
  <!-- form verify mobile number again -->
  <?php echo $msg1; ?>
  <form id="verify_again" method="post">
    <div class="form-group">
        <label for="verifymobile"><b>Change Number:</b></label>
      <input type="text" class="form-control" id="verifymobile" name="get_change_mobile_num" value="<?php echo $tfa_mobile_no; ?>">
    </div>
    <input type="submit" name="change_mobile_num_again" class="btn btn-primary" value="save"></button>
  </form>
  <br> 

  <!-- form verify otp and enable status -->
  <form method="post">
    <div class="form-group">
      <label for="verifymobile"><b>Enter OTP:</b></label>
      <input type="text" class="form-control" name="mobl_nmber_fortwofa_recieved_code" >
    </div>
    <input type="submit" name="get_code_to_verify_twofa" class="btn btn-primary" value="Verify Mobile"></button>
  </form>
  <br>

</div>
</div>
<script type="text/javascript">
    secs       = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock      = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = clock;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
        } else {
            document.getElementById(id).innerHTML = "Please resend OTP again";
            document.getElementById('verify_again').style.display = 'block';
            document.getElementById('resend_otp').style.display = 'block';
        }
    }

jQuery(document).ready(function () {
  setTimeout(function () {
    jQuery('#msg_ids').hide();
  }, 8000);
});
</script>

<?php wp_footer(); ?>
