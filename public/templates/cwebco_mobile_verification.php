<?php
/**
 * Template Name: Twofa Mobile Verification
 */

global $wpdb;
get_header();
if(!isset($_SESSION)) 
{
  session_start();
}
$current_user_id = get_current_user_id();
$token = wp_get_session_token();
$msg1="";
  if ( is_user_logged_in() ) 
  { 
   $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
   $token_id = get_user_meta($current_user_id, 'token_id', true);
   $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
   $tfa_saved_otp = base64_decode($tfa_saved_otp1);
   $author_obj = get_user_by('id', $current_user_id);
   $user_role = $author_obj->roles[0];
   $current_user_name = $author_obj->data->user_nicename;
   $current_user_id = $author_obj->data->ID;
   if(empty($token_id) && (empty($_POST))){
   }
  }else{
    $url = get_option('siteurl') . '';
    echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
    header("location:".$url);
   $current_user_id = $_SESSION['2fa_data_id'];
   $display_name = get_display_name($current_user_id);
   $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
   $author_obj = get_user_by('id', $current_user_id);
   $current_user_name = $author_obj->data->user_nicename;
   $current_user_id = $author_obj->data->ID;
  }

// Resend otp to number again
if(isset($_POST['resend_code_to_verify_twofa']))
{ 
  do_action( 'twilio_hook_reg', $tfa_mobile_no);
  echo "<script>localStorage.setItem('timer_over','');</script>";
}
 
// verify otp and enable status
if(isset($_POST['get_code_to_verify_twofa']))
{ 
  if (!empty( $_POST['mobl_nmber_fortwofa_recieved_code'] ))
  { 
    $mobile_otp =  $_POST['mobl_nmber_fortwofa_recieved_code'];
    if($tfa_saved_otp == $mobile_otp){
      update_user_meta( $current_user_id, 'user_2fa_enabled', 'yes');
      update_user_meta( $current_user_id, 'user_phone_verified', 'yes');
      update_user_meta( $current_user_id, 'token_id', $token);
      update_user_meta( $current_user_id, '2fa_otp', '');
      $table_name = $wpdb->prefix.'cwebco_2fa_success_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
      $table_name2 = $wpdb->prefix.'cwebco_2fa_enabled_users';
      do_action( 'insert_update_user_2fa_enb', $table_name2);
      unset($_SESSION['2fa_data_id']);      
      if($user_role == 'administrator'){
       $url = get_option('siteurl') . '/wp-admin/index.php';
      }else{
       //$url = get_option('siteurl') . '/wp-admin/profile.php';
       $url = get_option('siteurl') . '/twofa-accountpage/';
      }
      echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
      header("location:".$url);
    }else{
      $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
      $msg1 .= "<br><span id='msg_ids' style='color:red;' >OTP not correct.</span>";
    }   
  }else{
    $msg1 .= "<span id='msg_ids' style='color:red;' >Field Not Be Empty</span>";
  }
}
else
{
  $msg1="";
}
?>
<div class="custom_page_class">

<!-- form verify otp and enable status -->
<div class="center_back_otp">
<form method="post" id="otp_form"  class="hide_after_60 text_otp_cent">
  <div class="form-group">
    <label for="verifymobile"><b>Enter the 6-digit One Time Password (OTP) sent to your mobile number</b></label>  
    <input type="text" class="form-control" name="mobl_nmber_fortwofa_recieved_code" >
    <?php echo $msg1; ?>  
    <span id="cunt_seconds"> seconds</span><span id="countdown-1"><?php if(isset($_POST['countdown-input'])) echo $_POST['countdown-input']; else echo "120"; ?></span>
    <input type="hidden" class="form-control" name="countdown-input" id="countdown-input" value="<?php echo $_POST['countdown-input']; ?>">
  <input type="hidden" class="form-control" name="curent_user" id="curent_user" value="<?php echo get_current_user_id(); ?>">
  </div>
  <div class="btn_vrify">    
  <input type="submit" name="get_code_to_verify_twofa" class="btn btn-primary verify_mob" value="Submit">
    </div>  
    
</form>
<!-- form Resend otp to number again -->
<form style="display:block" id="resend_otp" method="post">
<div class="btn_vrify">     
  <input id="resend_otp_trigger" type="submit" name="resend_code_to_verify_twofa" class="btn btn-primary verify_mob" value="Resend OTP">
</div>
</form>
</div>
</div>
<script type="text/javascript">
    var timer_new = document.getElementById('countdown-input').value;
    var curent_user = document.getElementById('curent_user').value;
     if(timer_new ==''){
           timer_new=120;
     }else{
           timer_new=timer_new;
     }
    if(timer_new>0)
      localStorage.setItem('timer_over','');

    var time_over = localStorage.getItem("timer_over");
    if(time_over != "userlogin"){
    secs       = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    setTimeout("countdown('countdown-input',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock      = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = timer;
        if(id=='countdown-input')
        document.getElementById(id).value = timer;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
            setTimeout(localStorage.setItem("timer_over","userlogin"),1000);
        } else {
            localStorage.setItem("timer_over","userlogin");
            document.getElementById(id).innerHTML = "Please resend OTP again";
            document.getElementById('cunt_seconds').style.display = 'none';
            jQuery("input#countdown-input").val("null");
            var meta_id = '<?php echo $current_user_id; ?>'; 
            jQuery.ajax({
              type: 'post',
              data: {meta_id: meta_id},
              success: function(response){
                // Code
              }
            });
          
        }
    }
    }else{
      document.getElementById("cunt_seconds").innerHTML = "Please resend OTP again";
      
      document.getElementById('countdown-1').style.display = 'none';
      var meta_id = '<?php echo $current_user_id; ?>'; 
            jQuery.ajax({
              type: 'post',
              data: {meta_id: meta_id},
              success: function(response){
                // Code
              }
            });
    }

jQuery( document ).ready(function() {
            jQuery(document).on('click', '#wp-admin-bar-logout', function(){
            console.log('timer_over');
            localStorage.setItem('timer_over','abc');
            localStorage.removeItem("timer_over");
          });
        });
    
</script>
<?php 
if(isset($_POST['meta_id']) ){
  $delete_user_m_id = $_POST['meta_id'];
  update_user_meta( $delete_user_m_id, '2fa_otp', '');
  exit;
 }
 ?>
<?php wp_footer(); ?>
