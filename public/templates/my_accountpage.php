<?php
/**
 * Template Name: Twofa My account
 */
global $wpdb;
get_header();


if(!isset($_SESSION)) 
{
session_start();
}
$msg="";
$current_user_id = get_current_user_id();
$mobl_nmber_fortwofa = get_user_meta($current_user_id, 'user_phone_number', true);
$enble_disble_twofa_frntend = get_user_meta($current_user_id, 'enble_disble_twofa_frntend', true);
if(isset($_POST['save_usr_mobl_for_twofa']))
{
    if(empty($_POST['mobl_nmber_fortwofa'])) {
      $msg.= "<span id='msg_ids' style='color:red;' >Field Not Be Empty</span>";
      $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
    }else{
    if(preg_match("/^[+]{1}[01234567890]{2}[0-9]{10}$/", $_POST['mobl_nmber_fortwofa'])) {
    do_action( 'twilio_hook_reg', $_POST['mobl_nmber_fortwofa']);
    $_SESSION['2fa_mobile_no'] = $_POST['mobl_nmber_fortwofa'];
    $url = get_permalink( get_page_by_path( 'twofa-auth-after-signup' ) );
    header('Location:'. $url);
    exit;
    }else{
      $msg.= "<span id='msg_ids' style='color:red;' >Invalid phone number</span>";
      $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
    }
  }
}
else
{
  $msg="";
} 
 


if(isset($_POST['save_enble_disble_twofa_frntend']))
{
  if ( is_user_logged_in() )
  {
    update_user_meta($current_user_id, 'enble_disble_twofa_frntend',  $_POST['frontend_enble_disble_twofa']);
  }
}

?>
<div class="custom_page_class">
  <div class='form_outer'>
    <div class='twofamesg'><?php echo $msg; ?></div>
    <form id='registerForm' name='registerForm' class="validate" method="post">
      <div class="form-group">
          <label for="verifymobile"><b>You will have to verify your phone number for 2 Factor Authentication:</b></label>
        <input type="text" class="form-control" id="verifymobile" placeholder="" name="mobl_nmber_fortwofa" value="<?php echo $mobl_nmber_fortwofa; ?>" data-rule-mustbeyoungerthan="14" data-msg-mustbeyoungerthan="Invalid phone number.">
      </div>
     
      <input type="submit" name="save_usr_mobl_for_twofa" class="btn btn-primary" value="Proceed"></button>
    </form>
  </div>
  <br><br> 
</div>
<script>
jQuery(document).ready(function () {
  setTimeout(function () {
    jQuery('#msg_ids').hide();
  }, 8000);
  localStorage.setItem('timer_over', 'abc');
  window.localStorage.removeItem("timer_over");
});
</script>
<?php wp_footer(); ?>
