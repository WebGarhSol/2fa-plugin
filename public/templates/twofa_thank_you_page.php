<?php
/**
 * Template Name: Twofa Thank You
 */
get_header();
if(!isset($_SESSION))
{
	session_start();
}


if ( !is_user_logged_in() )
{ 
	wp_redirect(site_url());
    exit;
}
?>

<div style="width: 50%; margin: auto; margin-top: 50px;" class="custom_page_class">
<p>Thank you for verifying the phone number. The 2 Factor Auth. is enabled now. Everytime you login, you will have to enter the OTP. </p>
</br>
<a class="search-submit" href="<?php echo home_url(''); ?>">Home</a> | <a class="search-submit" href="<?php echo get_permalink( get_page_by_path( 'twofa-user-myaccount' ) ); ?>">My Account</a> 
</div>

<?php wp_footer(); ?>
