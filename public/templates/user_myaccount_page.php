<?php
/**
 * Template Name: Twofa User Myaccount
 */
get_header();
if(!isset($_SESSION))
{
  session_start();
}
global $wpdb;
$current_user_id = get_current_user_id();
$token = wp_get_session_token();
$msg1="";

if ( is_user_logged_in() )
{ 
 $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
 $user_2fa_enabled = get_user_meta($current_user_id, 'user_2fa_enabled', true);
 $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
 $tfa_saved_otp = base64_decode($tfa_saved_otp1);
}else{
  if(!isset($_SESSION['2fa_data_id']))
  {
    wp_redirect(site_url());
    exit;
  }
 $current_user_id = $_SESSION['2fa_data_id'];
 $tfa_mobile_no = get_user_meta($current_user_id, 'user_phone_number', true);
 $user_2fa_enabled = get_user_meta($current_user_id, 'user_2fa_enabled', true);
 $tfa_saved_otp1 = get_user_meta($current_user_id, '2fa_otp', true);
 $tfa_saved_otp = base64_decode($tfa_saved_otp1);
}
   
//verify mobile number again 
if(isset($_POST['verify_mobile_num_again']))
{ 
  if (!empty( $_POST['get_verify_mobile_num'] ))
  { 
    if(preg_match("/^[+]{1}[01234567890]{2}[0-9]{10}$/", $_POST['mobl_nmber_fortwofa'])) {
    $_SESSION['2fa_mobile_no'] = $_POST['get_verify_mobile_num'];
    do_action( 'twilio_hook_reg', $_POST['get_verify_mobile_num']); 
  }else{ 
    $msg1 .= "<span id='msg_ids' style='color:red;' >Invalid phone number</span>";
  }
  }else{
    $msg1 .= "<span id='msg_ids' style='color:red;' >Field Not Be Empty</span>";
  }
}

// Resend otp to number again
if(isset($_POST['resend_code_to_verify_twofa']))
{ 
  do_action( 'twilio_hook_reg', $tfa_mobile_no);
}

// verify otp and enable status
if(isset($_POST['get_code_to_verify_twofa']))
{ 
  if ( !empty($_POST['mobl_nmber_fortwofa_recieved_code'] ))
  { 
    $mobile_otp =  $_POST['mobl_nmber_fortwofa_recieved_code'];
    if($tfa_saved_otp == $mobile_otp){
      update_user_meta( $current_user_id, 'user_2fa_enabled', 'yes');
      update_user_meta( $current_user_id, 'user_phone_verified', 'yes');
      update_user_meta( $current_user_id, 'token_id', $token);
      $table_name = $wpdb->prefix.'cwebco_2fa_success_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
      unset($_SESSION['2fa_data_id']);
      $url = home_url( 'index.php' ).'';
      echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
    }else{
      $table_name = $wpdb->prefix.'cwebco_2fa_failed_auth_attempt';
      do_action( 'insert_update_user_2fa', $table_name);
    }   
  }else{
    $msg1 .= "<span id='msg_ids' style='color:red;' >Field Not Be Empty</span>";  
  } 
}
else
{
  $msg1="";
}

?>

<div style="width: 50%; margin: auto; margin-top: 50px;" class="custom_page_class">
<?php echo $msg1; ?>
<form method="post">
  <div class="form-group">
    <label for="verifymobile"><b>Mobile Nubmer:</b></label>
    <input readonly type="text" class="form-control" id="verifymobile" name="mobl_nmber_fortwofa" value="<?php echo $tfa_mobile_no; ?>">
  </div>
  <a class="search-submit" href="#">Change mobile nubmer</a>
  <div class="form-group">
    <label for="verifymobile"><b>2FA Status: </b></label>
    <?php if($user_2fa_enabled == 'yes'){ ?> <b>yes</b>
    <?php } else { ?> <b>No</b> <?php } ?>
  </div>
</form>
</div>


<?php get_footer(); ?>
