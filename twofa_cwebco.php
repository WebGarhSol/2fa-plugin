<?php
ob_start();
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wp-plugins.co.in
 * @since             1.0.0
 * @package           karge-catalogue
 *
 * @wordpress-plugin
 * Plugin Name:       cWebco Two Factor Authentication
 * Description:       The plugin allows you to enable 2FA for the selected user roles. The Twilio is being used to send the OTP to the users.
 * Version:           1.0.0
 * Author:            cWebco
 * Author URI:        http://wp-plugins.co.in
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       twofa_cwebco
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}



/* Plugin Name */
$cwebPluginName="cWebco Two Factor Authentication";

/* Use Domain as the folder name */
$PluginTextDomain="twofa_cwebco";


/**
 * The code that runs during plugin activation.
*/
function activate_tfa_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/classes/activate-class.php';
	classes_cw\Plugin_Activator::activate();
}
/**
 * The code that runs during plugin deactivation.
*/
function deactivate_tfa_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/classes/deactive-class.php';
	classes_cw\Plugin_Deactivator::deactivate();
}

/* Register Hooks For Start And Deactivate */
register_activation_hook( __FILE__, 'activate_tfa_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_tfa_plugin' );

/**
 * The core plugin class that is used to define internationalization,
*/
require plugin_dir_path( __FILE__ ) . 'includes/classes/classCweb.php';

/*Include the Files in which we define the sortcodes for front End */
require plugin_dir_path( __FILE__ ) . 'public/short-codes.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {
	$plugin = new cWebClasstfa();
	$plugin->run();
}
run_plugin_name();

/* Constant */
define('CWEB_TFADP_PATH', plugin_dir_path(__FILE__) ); // CWEB_TFADP_PATH
define('CWEB_TFADU_PATH', plugin_dir_url(__FILE__) ); // CWEB_TFADU_PATH

/*
 * Include Custom Feild Files
 */

//Declares Common Function File 



require plugin_dir_path( __FILE__ ) . 'includes/function/fucntions.php';


require plugin_dir_path( __FILE__ ) . 'includes/function/custom-plugin-funtion.php';
